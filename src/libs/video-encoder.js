let uuid = require('uuid').v4
const delay = require('delay');
let statusDB = {}

exports.IVideoEncoder = IVideoEncoder


async function IVideoEncoder(userId, s3_mp4Uri)
{
    let taskId = userId + uuid();
    let s3Uri = taskId + ".mp4"
    
    // Fire the encoder and give the api a task id
    encodeVideo(taskId, s3_mp4Uri, s3Uri)
    statusDB[taskId] = "encoding"
    return taskId
}

exports.IStatusCheck = IStatusCheck

async function IStatusCheck(taskId)
{
    return statusDB[taskId]
}

async function encodeVideo(taskId, inputS3, s3Uri) {
    
    let encodedVideo = await ffmpegEncoder(inputS3)
    await saveToS3(encodedVideo, s3Uri)
    statusDB[taskId] = "completed"

}
async function saveToS3(mp4ByteArray, outputS3) { }

async function ffmpegEncoder(inputS3) { 
    await delay(10000)
}