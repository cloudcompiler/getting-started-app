exports.IGetUserProfileService = IGetUserProfileService

async function IGetUserProfileService(userName)
{
    // Connect to a database, caching, [...]
    let alice = {
        Name: "Alice",
        Expertise: "Crypto"
    }

    let bob = {
        Name: "Bob",
        Expertise: "Communication"
    }

    return userName == "Alice"? alice : bob
    
}