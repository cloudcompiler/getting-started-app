let videoEnc = require('./libs/video-encoder')

let userService = require('./libs/user-service')

const express = require('express')
const app = express()
var router = express.Router();
setupExpressRouter()
app.use('/', router);

/**
 * @capability https_server
 */
exports.app = app

router.post('/v1/status', async (req, res) => {
    console.log("got a status request")
    res.status(200).send({"message": "CloudCompiler coming for your lunch!"})
});

router.post('/v1/get-user', async (req, res) => {
    let userId = req.query.userId;
    console.log(userId)
    let userProfile = await userService.IGetUserProfileService(userId)
    res.send(userProfile)
});
router.post('/v1/encode-video', async (req, res) => {
    let userId = req.query.userId;
    console.log(req.query)
    let videoToEncodeURI = req.query.s3mp4;
    let taskId = await videoEnc.IVideoEncoder(userId, videoToEncodeURI)
    res.send({taskId: taskId})
})

router.post('/v1/check-status', async (req, res) => {
    let taskId = req.query.taskId;
    let videoToEncodeURI = req.query.s3mp4;
    let status = await videoEnc.IStatusCheck(taskId) || "Not Found"
    res.send({status: status})

})

if (process.env.NODE_ENV == "local") app.listen(3000, () => { console.log("Running Locally Ma'!") })


function setupExpressRouter() {
    const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
    const cors = require('cors')
    const bodyParser = require('body-parser')
    router.use(cors())
    router.use(bodyParser.json())
    router.use(bodyParser.urlencoded({ extended: true }))
    router.use(awsServerlessExpressMiddleware.eventContext())
}
