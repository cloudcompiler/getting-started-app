let uuid = require('uuid').v4
exports.IGetUserProfileService = IGetUserProfileService
let kvStorage = require('./key-value-service')

async function IGetUserProfileService(userName)
{
    let kvResult = await kvStorage.ISet(`user.${userName}.seen`, true)

    // Connect to a database, caching, [...]
    let alice = {
        Name: "Alice",
        Expertise: "Crypto"
    }

    let bob = {
        Name: "Bob",
        Expertise: "Communication"
    }

    return userName == "Alice"? alice : bob
    
}