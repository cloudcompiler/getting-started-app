const delay = require('delay');
const maxGetTokens = 3
// Redis in a real application
let distributedKV = {}

exports.IGet = IGet
exports.ISet = ISet

const execCountKey = `executionCount`
distributedKV[execCountKey] = 0

async function IGet(key) {
    let value = distributedKV[key]
    await delay(20)
    distributedKV[execCountKey] = distributedKV[execCountKey] + 1
    const execCount = distributedKV[execCountKey]
    return value ?
        { value: value, execCount: execCount } :
        { value: "", execCount: execCount }
}

async function ISet(key, value) {
    let userSetTokenKeys = `executionCount`
    distributedKV[key] = value
    distributedKV[execCountKey] = distributedKV[execCountKey] + 1
    await delay(50)
    return {
        statusMessage: "Success",
        execCount: distributedKV[execCountKey]
    }

}